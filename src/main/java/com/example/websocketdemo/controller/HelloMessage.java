package com.example.websocketdemo.controller;

import lombok.Data;

@Data
public class HelloMessage {
    private String name;
}
