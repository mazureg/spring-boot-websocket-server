package com.example.websocketdemo.controller;

import com.example.websocketdemo.model.ChatMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

/**
 * Created by rajeevkumarsingh on 24/07/17.
 */
@Controller
public class ChatController {

    @MessageMapping("/hello")
    @SendTo("/topic/public")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
        return chatMessage;
    }

/*
    @MessageMapping("/hello")
    @SendTo("/topic/public")
    public ChatMessage greeting(HelloMessage message) throws Exception {
        System.out.println("Received hello: " + message.getName());
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setContent("koza");
        chatMessage.setSender("milosz");
        chatMessage.setType(ChatMessage.MessageType.CHAT);
        return chatMessage;
    }
*/

}
